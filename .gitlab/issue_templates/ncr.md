Nonconformance Report
=====================

**Project Name:** PHASMA

**NCR title:** {{ncr_title}}

Subsystem: {{subsystem_id}} <!-- add subsystem -->

Supplier: {{supplier_name}} <!-- remove if not applicable -->

Purchase Order: {{purchase_order}} <!-- remove if not applicable -->

* NCR-N°: {{ncr_number}} <!-- add issue number after posting -->
* Revision: {{ncr_revision}}

<!--please add related NCRs if any as linked items-->

NC Observation
--------------

Date: {{date}}

Location: {{location}}

Description of Nonconformance: {{description_of_nonconformance}}

NC detected during: {{nc_detected_during}}

Requirements violated: {{requirements_violated}}

Classification:

- [ ] Minor
- [ ] Major

Classification and Cause
------------------------

Verification: {{verification}}

Cause of NC: {{cause_of_nc}}

Ref. to MoMs: {{ref_to_moms}}

Finally determined Cause of NC: {{finally_determined_cause_of_nc}}

Actions
-------

Corrective/Preventive Actions: {{corrective_preventive_actions}}

Ref to Failure Report: {{ref_to_failure_report}}

Request for Waiver:

- [ ] No
- [ ] Yes Reference: {{reference}}

Actions:

- [ ] Return to supplier 
- [ ] Use “as-is”
- [ ] Rework
- [ ] Repair
- [ ] Scrap
- [ ] Nothing

/label ~ncr