reset
#Uncomment to write to png file
set terminal png
set output 'phasma-primus.tumb.png'
set title "PHASMA-PRIMUS\nCROC - Cross-section of Complex Bodies\ncross-section under different aspect angles"
set xlabel "Azimuth [deg]"
set ylabel "Elevation [deg]"
set grid
set xrange [-180:180]
set yrange [-90:90]
set format cb "%g"
set pm3d map
unset key
# Matlab-style palette
set palette defined ( 0 '#000090',  1 '#000fff', 2 '#0090ff',3 '#0fffee', 4 '#90ff70', 5 '#ffee00', 6 '#ff7000', 7 '#ee0000', 8 '#7f0000')
splot 'phasma-primus.tumb' u 1:2:3 with points palette ps 2 pt 7
