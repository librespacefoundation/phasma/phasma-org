reset
set term png
set output "phasma-gnt.osg.png"
set xdata time
set timefmt "%d/%m/%Y"
set format x "%Y"
set title "PHASMA-GNT\nNominal attitude 10deg pitch\nSolar activity vs. Time"
set xlabel "Date"
set ylabel "Solar activity [sfu]"
set xrange [*:*]
set yrange [*:*]
set format y "%5.1f"
set key below
plot \
"phasma-gnt.osg" u 1:2 w p lt 01 lw 01 title "Daily F10.7", \
"phasma-gnt.osg" u 1:3 w l lt 02 lw 01 title "Avg. F10.7 (Last 81 days)"
