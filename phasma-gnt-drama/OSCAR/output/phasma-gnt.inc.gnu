reset
set term png
set output "phasma-gnt.inc.png"
set xdata time
set timefmt "%Y-%m-%d %H:%M:%S"
set format x "%Y"
set title "PHASMA-GNT\nNominal attitude 10deg pitch\nInclination vs. Time"
set xlabel "Date"
set ylabel "Inclination [deg]\nSingly averaged (over M)"
set xrange [*:*]
set yrange [*:*]
set format y "%6.2f"
set key below
plot \
"phasma-gnt.oev" u 1:6 w l lt 01 lw 01 notitle
